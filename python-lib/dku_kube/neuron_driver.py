import os, json, logging

from dku_aws.eksctl_command import EksctlCommand
from .kubectl_command import run_with_timeout

def has_neuron_ds(kube_config_path):
    env = os.environ.copy()
    env['KUBECONFIG'] = kube_config_path
    cmd = ['kubectl', 'get', 'pods', '--namespace', 'kube-system', '-l', 'name=neuron-device-plugin-daemonset', '--ignore-not-found']
    logging.info("Checking if Neuron daemonset is installed with: %s" % json.dumps(cmd))
    out, err = run_with_timeout(cmd, env=env, timeout=5)
    return len(out.strip()) > 0

def add_neuron_ds_if_needed(cluster_id, kube_config_path, connection_info):
    if not has_neuron_ds(kube_config_path):
        for url in (
            "https://awsdocs-neuron.readthedocs-hosted.com/en/latest/_downloads/46fb1da6e5e79c3310ebc0cbd6ad2353/k8s-neuron-device-plugin-rbac.yml",
            "https://awsdocs-neuron.readthedocs-hosted.com/en/latest/_downloads/f57f27621e52b305dba7d624c477977a/k8s-neuron-device-plugin.yml",
        ):
            cmd = ['kubectl', 'apply', '-f', url]
            logging.info("Install Neuron daemonset with: %s" % json.dumps(cmd))
            env = os.environ.copy()
            env['KUBECONFIG'] = kube_config_path
            run_with_timeout(cmd, env=env, timeout=5)

